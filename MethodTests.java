public class MethodTests
{
	public static void main(String[] args)
	{
		int x = 10;
		System.out.println(x);
		
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		
		methodOneInputNoReturn(x);
		
		methodOneInputNoReturn(x + 50);
		
		methodTwoInputsNoReturn(x, 3.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double squareRoot = sumSquareRoot(6, 3);
		System.out.println(squareRoot);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass numPlusTwo = new SecondClass();
		System.out.println(numPlusTwo.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int incomingNumber)
	{
		System.out.println("Inside the method one input no return");
		System.out.println(incomingNumber);
	}
	
	public static void methodTwoInputsNoReturn(int someIntVar, double someDoubleVar)  // PART 2, Q6: Should we let it print the input specified in the main when this method is called?
	{
		System.out.println("Inside the method two inputs no return");
		System.out.println(someIntVar);
		System.out.println(someDoubleVar);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 6;
	}
	
	public static double sumSquareRoot(int firstNum, int secondNum)
	{
		int sumNumbers = firstNum + secondNum;
		double squareRtSum = Math.sqrt(sumNumbers);
		return squareRtSum;
	}
}