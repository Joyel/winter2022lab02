import java.util.Scanner;

public class PartThree
{
	public static void main(String[] args)
	{
		Scanner read = new Scanner(System.in);
		
		System.out.println("Enter the length of the square's side.");
		double squareSide = read.nextDouble();
		
		double areaSquare = AreaComputations.areaSquare(squareSide);
		
		System.out.println("Enter the length of the rectangle.");
		double recLength = read.nextDouble();
		
		System.out.println("Enter the width of the rectangle.");
		double recWidth = read.nextDouble();
		
		AreaComputations recArea = new AreaComputations();
		
		double areaRectangle = recArea.areaRectangle(recLength, recWidth);
		
		System.out.println("The area of your square is " + areaSquare);
		System.out.println("The area of your rectangle is " + areaRectangle);
	}
}