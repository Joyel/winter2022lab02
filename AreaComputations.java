public class AreaComputations
{
	public static double areaSquare(double squareSide)
	{
		return Math.pow(squareSide,2);
	}
	
	public double areaRectangle(double recLength, double recWidth)
	{
		return recLength * recWidth;
	}
}