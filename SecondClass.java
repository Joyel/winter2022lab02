public class SecondClass
{
	public static int addOne(int someNumber)
	{
		someNumber++;
		return someNumber;
	}
	
	public int addTwo(int someNumber)
	{
		someNumber += 2;
		return someNumber;
	}
	
}